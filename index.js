//getter and setter of local storage
const get = (id) => JSON.parse(localStorage.getItem(id))
const set = (id, data) => localStorage.setItem(id, JSON.stringify(data))
//initialize sample data
get('users') || set('users', {
    'abc': {
        "email": "abc@abc.com",
        "uname": "abc",
        "pass": "abc",
        "fname": "abc",
        "lname": "cba",
        "gender": "male",
        "role": "admin"
    },
    'def': {
        "email": "def@abc.com",
        "uname": "def",
        "pass": "def",
        "fname": "def",
        "lname": "def",
        "gender": "male",
        "role": "admin"
    },
    'sda': {
        "email": "sda@abc.com",
        "uname": "sda",
        "pass": "sda",
        "fname": "sda",
        "lname": "sda",
        "gender": "female",
        "role": "operations"
    },
    'fgg': {
        "email": "fgg@abc.com",
        "uname": "fgg",
        "pass": "fgg",
        "fname": "fgg",
        "lname": "fgg",
        "gender": "female",
        "role": "operations"
    },
    'tty': {
        "email": "tty@abc.com",
        "uname": "tty",
        "pass": "tty",
        "fname": "tty",
        "lname": "tty",
        "gender": "male",
        "role": "sales"
    },
    'xyz': {
        "email": "xyz@abc.com",
        "uname": "xyz",
        "pass": "xyz",
        "fname": "xyz",
        "lname": "xyz",
        "gender": "female",
        "role": "sales"
    }
})

let self
//called on load of  body
function load() {
    self = get('self')
    if (!self) {
        document.getElementById("p1").style.display = 'none'
        document.getElementById("p2").style.display = 'block'
        return
    }
    else {
        document.getElementById("p1").style.display = 'block'
        document.getElementById("p2").style.display = 'none'

        show()
    }

}
//sign-up form
function signup() {
    event.preventDefault()
    let email = document.getElementById('signup_email').value
    let uname = document.getElementById('signup_uname').value
    let pass = document.getElementById('signup_pass').value
    let fname = document.getElementById('signup_fname').value
    let lname = document.getElementById('signup_lname').value
    let gender = document.getElementById('female').checked ? "female" : "male"
    let role = document.getElementById('role').value

    if (getUname(uname)) {
        showError("This username already exists <br> Try different Username")
        document.getElementById("close_signup").click()
        return
    }
    console.log(email, uname, pass, fname, lname, gender, role)
    data = { email, uname, pass, fname, lname, gender, role }
    let users = get("users")

    set('self', data)
    let newUsers = { ...users }
    newUsers[uname] = data
    set('users', newUsers)

    window.location.reload()

}
//login form
function login() {
    event.preventDefault()

    let uname = document.getElementById('login_uname').value
    let pass = document.getElementById('login_pass').value
    let key = getUname(uname)
    if (!key) {
        showError("username & password may be invalid")
        document.getElementById("close_login").click()

        return
    }
    if (!(key.uname == uname && key.pass == pass)) {
        showError("username password invalid")
        document.getElementById("close_login").click()

        return
    }
    set('self', key)
    window.location.reload()
}

//display controller according to authorizations
function show() {
    let users = Object.values(get("users"))
    let self = get('self')

    let operations = users.filter(u => u.role == 'operations')
    let sales = users.filter(u => u.role == 'sales')
    let admins = users.filter(u => u.role == 'admin')

    console.log(operations, sales, admins)
    switch (self.role) {
        case 'admin': showAdmins(admins)
        case 'operations': showOperations(operations)
        case 'sales': showSales(sales)

    }


}
//get user by username
function getUname(uname) {
    let users = get("users")
    return users[uname]
}
// logout and remove self 
function logout() {
    localStorage.removeItem('self')
    window.location.reload()

}
//displaying sales
function showSales(sales) {

    document.getElementById('show_sales').innerHTML = ''
    temp = sales.length ? '' : "Empty"
    for (i in sales) {
        let sale = sales[i]
        temp += `<tr>
        <td>
        ${Number(i) + 1}
        </td>
        <td>
        ${sale.fname}
        </td>
        <td>
        ${sale.lname}
        </td>
        <td>
        ${sale.email}
        </td>
        <td>
        ${sale.uname}
        </td>
        <td>
        ${sale.gender}
        </td>
        </tr>
        `
    }
    console.log(temp)
    document.getElementById('show_sales').innerHTML = temp

}
//displaying operations
function showOperations(operations) {

    document.getElementById('show_operations').innerHTML = ''
    temp = operations.length ? '' : "Empty"
    for (i in operations) {
        let opp = operations[i]
        temp += `
        <tr>
        <td>
        ${Number(i) + 1}
        </td>
        <td>
        ${opp.fname}
        </td>
        <td>
        ${opp.lname}
        </td>
        <td>
        ${opp.email}
        </td>
        <td>
        ${opp.uname}
        </td>
        <td>
        ${opp.gender}
        </td>
        </tr>
        `
    }
    document.getElementById('show_operations').innerHTML = temp
}
//displaying admins
function showAdmins(admins) {
    document.getElementById('show_admins').innerHTML = ''
    let temp = ''
    for (i in admins) {
        let admin = admins[i]
        temp += `<tr>
        <td>
        ${Number(i) + 1}
        </td>
        <td>
        ${admin.fname}
        </td>
        <td>
        ${admin.lname}
        </td>
        <td>
        ${admin.email}
        </td>
        <td>
        ${admin.uname}
        </td>
        <td>
        ${admin.gender}
        </td>
        </tr>
        `
    }
    document.getElementById('show_admins').innerHTML = temp
}

function showError(err) {
    document.getElementById("error").innerHTML = err
    setTimeout(() => document.getElementById("error").innerHTML = ''
        , 3000)
}

document.getElementById("close_signup").onclick = () => {
    document.getElementById('signup_email').value = ''
    document.getElementById('signup_uname').value = ''
    document.getElementById('signup_pass').value = ''
    document.getElementById('signup_fname').value = ''
    document.getElementById('signup_lname').value = ''

}

document.getElementById("close_login").onclick = () => {
    document.getElementById('login_uname').value = ""
    document.getElementById('login_pass').value = ""
}

